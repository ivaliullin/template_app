= Инструкция по настройке машины разработчика
:toc-title: Оглавление
:hardbreaks:
:experimental:
:toc:

== Предварительные условия

* При написании статьи использовалась Linux Mint 19.3, также проверялось на 20.0

== Операции

=== Установка утилит и зависимостей

1.{sp}Установить пакеты зависимостей и утилит.
[source]
----
$ sudo apt-get install curl \
                       apt-transport-https ca-certificates gnupg-agent software-properties-common
----
* 2-я строка – утилиты для установки docker. (apt-transport-https, ...)

2.{sp}Добавяем репозитории.
[source]
----
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu <version_name> stable" <1>
$ sudo add-apt-repository ppa:wireshark-dev/stable
----
<1> <version_name> - имя версии (например, для mint 19.x - bionic, для mint 20.x - focal)

3.{sp}Добавить ключ для docker
[source]
----
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
----

4.{sp}Обновить список пакетов
[source]
----
$ sudo apt-get update
----

5.{sp}Установить пакеты.
[source]
----
$ sudo apt-get install nano zip unzip tree wget vim wireshark \
                       supervisor \
                       docker-ce docker-ce-cli containerd.io \
                       unixodbc-dev libssl-dev automake autoconf libncurses5-dev libwxgtk3.0-dev g++ \
                       git kdiff3 emacs \
                       cmake libssh-dev libsmbclient-dev libnfs-dev libpcre3-dev libarchive-dev
----
* 1-я строка – общие утилиты. (nano, ...)
** При установке wireshark выбираем "да" - для возможности пользователям группы wireshark выполнять захват пакетов.
* 2-я строка – supervisor, необходим для запуска r-ки.
* 3-я строка – docker, необходим для сборки проектов (rtx, web приложений), также используется для разворота PostgreSQL. (docker-ce, ...)
* 4-я строка – утилиты для сборки erlang через kerl. (для mint 20.x указать libwsgtk3.0-gtk3-dev вместо libwxgtk3.0-dev)
* 5-я строка – git и т.д..
* 6-я строка – зависимости для сборки far2l.

6.{sp}Скачать rebar.
[source]
----
$ mkdir -p ~/bin && cd ~/bin && curl -OL https://github.com/rebar/rebar/wiki/rebar
----

7.{sp}Установить права на файл
[source]
----
$ chmod a+x rebar
----

8.{sp}Добавить текущего пользователя в группу docker и wireshark
[source]
----
$ sudo usermod -aG docker,wireshark $USER
----

9.{sp}Перелогинить десктопную сессию. Возможно потребуется перезагрузка (для cinnamon). Выполнить в терминале groups и убедиться в наличии в списке docker и wireshark.

=== Установка и настройка PostgreSQL

PostgreSQL может быть установлена на другом сервере.

1.{sp}Создаем том для контейнера PostgreSQL
[source]
----
$ sudo docker volume create pg_data_vol
----

2.{sp}Запускаем контейнер PostgreSQL. Пароль от пользователя postgress можно установить в параметре POSTGRES_PASSWORD. Контейнер запускается с сетью host, поэтому проброс портов не нужен.
[source]
----
sudo docker run \
            --name postgre_local \
            -v pg_data_vol:/var/lib/postgresql/data \
            -e POSTGRES_PASSWORD=123 \
            -dit \
            --restart unless-stopped \
            --network=host \
            postgres:10
----

3.{sp}Подключаемся к контейнеру.
[source]
----
$ sudo docker exec -it postgre_local bash
----

4.{sp}Подключаемся к PostgreSQL.
[source]
----
# psql -U postgres
----

5.{sp}Создаем пользователя для r-ки.
[source]
----
CREATE ROLE rostellpgadmin LOGIN ENCRYPTED PASSWORD '123456' SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION;
----

6.{sp}Выходим в хост.
[source]
----
# \q

далее

# ctrl+d
----

=== Установка kerl (erlang)

1.{sp}Скачиваем kerl.
[source]
----
$ mkdir -p ~/bin && cd ~/bin && curl -OL https://raw.githubusercontent.com/kerl/kerl/master/kerl
----

2.{sp}Устанавливаем права на файл.
[source]
----
$ chmod a+x kerl
----

3.{sp}Обновляем профиль для текущего терминала.
[source]
----
$ source ~/.profile
----

4.{sp}Обновляем список доступных релизов erlang для установки.
[source]
----
$ kerl update releases
----

4.{sp}Билдим erlang (на момент написания статьи использовался erlang=20.3).
[source]
----
$ kerl build 20.3
----

5.{sp}Установить erlang в произвольную директорию.
[source]
----
$ mkdir -p ~/erlangs && kerl install 20.3 ~/erlangs/20.3
----

6.{sp}Пропатчить emacs в erlang для форматирования через intelliji idea
[source]
----
$ nano ~/erlangs/20.3/lib/tools-2.11.2/emacs/erlang.el
----
. ctrl+w (поиск в тексте) -> setq comment-column 48
.. изменяем значение 48 -> 0
. ctrl+w (поиск в тексте) -> looking-at "%%"
.. изменяем значение "%%" -> "%"
. ctrl+O (сохраняем) -> enter

=== Установка и настройка R

1.{sp}Клонируем репозиторий r.
[source]
----
$ mkdir -p ~/develop && cd ~/develop && git clone https://bitbucket.org/rtrep/r.git && mkdir -p r/rostell_ws/priv/www
----

2.{sp}Копируем инсталлер в корень склонированного репозитория.
[source]
----
$ cd ~/develop/r && cp -p rostell_cfg/priv/sysscripts/rostell1.escript rostellinstaller
----

3.{sp}Копируем sample assembly.info.hrl для билда.
[source]
----
$ cp ~/develop/r/rostell_env/include/r_env_assembly_info.hrl.sample ~/develop/r/rostell_env/include/r_env_assembly_info.hrl
----

4.{sp}Клонируем репозиторий product-xunit.
[source]
----
$ cd ~/develop && git clone https://bitbucket.org/rtrep/product-xunit.git
----

5.{sp}Создаем симлинки из каталога r для метаданных продукта xunit.
[source]
----
$ mkdir -p ~/develop/r/rostell_env/priv && \
    rm -rf ~/develop/r/rostell_env/priv/rest_metadata && ln -s ~/develop/product-xunit/assets/rostell_env/priv/rest_metadata ~/develop/r/rostell_env/priv/rest_metadata
----

6.{sp}Клонируем репозиторий r_assets.
[source]
----
$ cd ~/develop && git clone https://bitbucket.org/rtrep/r_assets.git
----

7.{sp}.Копируем ассеты в каталог с R.
[source]
----
$ cp -r ~/develop/r_assets/all/* ~/develop/r
----

8.{sp}Активируем erlang для текущей сессии shell.
[source]
----
$ . ~/erlangs/20.3/activate
----

Здесь можно создать и использовать алиасы команд для bash:
[source]
----
nano $HOME/.bash_aliases

alias rrnc="sudo $HOME/develop/r/scripts/r_role_node_connector.sh"
alias erl20=". $HOME/erlangs/20.3/activate"

. ctrl+O (сохраняем) -> enter
----

9.{sp}Билдим r-ку.
[source]
----
$ cd ~/develop/r && make
----

10.{sp}Создаем линк эрланга из kerl в каталог r-ки (т.к. r-ка использует erlang, который находится в её каталоге)
[source]
----
$ ln -s ~/erlangs/20.3 ~/develop/r/erlang
----

11.{sp}Устанавливаем систему.

[source]
----
$ sudo --preserve-env=PATH ./rostellinstaller install starttype=firstinit \ <1>
                  srvip=<ip_addr> \ <2>
                  srvname=<srvname> \ <3>
                  psk=<psk> \ <4>
                  sqlstr=<connection_string> \ <5>
                  gendomain=<general_domain> \ <6>
                  isdev=true <7>
----
<1> --preserve-env=PATH – сохраняем переменную окружения PATH для проброса пути до erlang (через kerl) в рамках выполнения с правами суперпользователя.
<2> <ip_addr> – ipv4 адрес доступный на машине
<3> <srvname> – произвольное имя сервера
<4> <psk> – произвольные символы, используется в erlang cookie
<5> <connection_string> – строка подключения к ранее установленной PostgreSQL
<6> <general_domain> – мастер домен
<7> isdev – признак установки в режиме разработчика

.Например
[source]
----
$ sudo --preserve-env=PATH ./rostellinstaller install starttype=firstinit \
                  srvip=192.168.10.141 \
                  srvname=testserv \
                  psk=123qwe \
                  sqlstr=host:192.168.10.141,port:5432,login:rostellpgadmin,pwd:123456,database:postgres \
                  gendomain=ceceron.ru \
                  isdev=true
----

=== Установка rtx

1.{sp}Клонируем репозиторий ci.
[source]
----
$ mkdir -p ~/develop && cd ~/develop && git clone https://bitbucket.org/rtrep/ci.git
----

2.{sp}Клонируем репозиторий rtx.
[source]
----
$ mkdir -p ~/develop && cd ~/develop && git clone https://bitbucket.org/rtrep/rtx.git
----

3.{sp}Клонируем репозиторий rtx_assets.
[source]
----
$ mkdir -p ~/develop && cd ~/develop && git clone https://bitbucket.org/rtrep/rtx_assets.git
----

4.{sp}Билдим контейнер для сборки rtx.
[source]
----
$ cd ~/develop/ci/pipeline/rtx_branch && ./build_container.sh
----

5.{sp}Билдим rtx (без запуска тестов).
[source]
----
$ SKIP_RTX_TESTS=true ./build_project.sh
----

6.{sp}Создаем симлинк из каталога r для rostell_mg.
[source]
----
$ mkdir -p ~/develop/r/rostell_mg/priv && ln -s ~/develop/rtx/_build ~/develop/r/rostell_mg/priv/unix
----

7.{sp}Создаем симлинк из каталога r для rostell_mixer.
[source]
----
$ mkdir -p ~/develop/r/rostell_mixer/priv && ln -s ~/develop/rtx/_build ~/develop/r/rostell_mixer/priv/unix
----

Процедура возможна для rtx с develop или версии > 3.11.0

Для обновления версии rtx необходимо выполнить pull репозитория rtx (или переключиться на нужную ветку) и выполнить шаг 5.

=== Установка web приложений

1.{sp}Клонируем репозиторий ci (если не был склонирован ранее)
[source]
----
$ mkdir -p ~/develop && cd ~/develop && git clone https://bitbucket.org/rtrep/ci.git
----

2.{sp}Билдим контейнер для сборки web приложений.
[source]
----
$ cd ~/develop/ci/pipeline/nodejs_build && ./build_container.sh
----

Дальнейшие шаги выполняются для каждого web приложения.

Список используемых веб приложений на момент написания статьи:
* app-root
* app-object
* app-configuration
* app-scripteditor
* webapp-monitor
* webapp-objects
* webapp-selector
* webapp-autoprovision
* webapp-webphone

2.{sp}Клонируем репозиторий веб приложения.
[source]
----
$ mkdir -p ~/develop && cd ~/develop && git clone https://bitbucket.org/rtrep/app-root.git
----
(Убедиться, что checkout ветка develop)

3.{sp}Билдим веб приложение.
[source]
----
$ cd ~/develop/ci/pipeline/nodejs_build && PROJECT=app-root ./build_project.sh
----

4.{sp}Создаем симлинк из каталога r для веб приложения.
[source]
----
$ mkdir -p ~/develop/r/rostell_ws/priv/www && ln -s ~/develop/app-root/_build ~/develop/r/rostell_ws/priv/www/app-root
----

Шаги 2-4 повторяются для каждого веб приложения. В завершение проверить, что симлинки замкнулись правильно:
[source]
----
ls -aslt ~/develop/r/rostell_ws/priv/www
----

=== Установка и настройка GitExtension

Выполняется по инструкции: https://github.com/gitextensions/gitextensions/wiki/How-To%3A-run-Git-Extensions-on-Linux[]

1.{sp}Устанавливаем mono-complete.
[source]
----
$ sudo apt install gnupg ca-certificates
$ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
$ echo "deb [arch=amd64] https://download.mono-project.com/repo/ubuntu stable-<version_name> main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
$ sudo apt-get update
$ sudo apt-get install mono-complete
----
<1> <version_name> - имя версии (например, для mint 19.x - bionic, для mint 20.x - focal)


2.{sp}Скачиваем GitExtensions.
[source]
----
$ mkdir ~/garbage && cd ~/garbage && curl -OL https://github.com/gitextensions/gitextensions/releases/download/v2.51.05/GitExtensions-2.51.05-Mono.zip
----

3.{sp}Распаковываем архив GitExtensions.
[source]
----
$ unzip -q GitExtensions-2.51.05-Mono.zip
----

4.{sp}Перемещаем в каталог приложений, удаляем лишний плагин (так указано в инструкции) и назначаем права исполняемому файлу.
[source]
----
$ mkdir ~/apps && mv GitExtensions ~/apps
$ cd ~/apps/GitExtensions && rm Plugins/Bitbucket.dll
$ chmod a+x gitext.sh
----

5.{sp}Создаем ярлык для запуска.
[source]
----
echo '[Desktop Entry]
Encoding=UTF-8
Exec=/home/<username>/apps/GitExtensions/gitext.sh <1>
Icon=/home/<username>/apps/GitExtensions/git-extensions-logo-final-256.ico <1>
Type=Application
Terminal=false
Comment=GitExtensions
Name=GitExtensions
GenericName=gitextensions
StartupNotify=false
Categories=Development;' | sudo tee /usr/share/applications/gitextensions.desktop
----
<1> <username> – необходимо подставить пользователя под которым происходила установка GitExtensions
Возможно потребуется перезагрузка (для cinnamon, если в приложениях не обнаруживается).

=== Установка far2l

1.{sp}Создаем папку git и клонируем в неё репозиторий far2l.
[source]
----
$ mkdir -p ~/git && cd ~/git && git clone https://github.com/elfmz/far2l
----

2.{sp}Переходим в директорию репозитория и создаем папку для билда.
[source]
----
$ cd far2l && mkdir build && cd build
----

3.{sp}Конфигурируем сборку.
[source]
----
$ cmake -DUSEWX=yes -DCMAKE_BUILD_TYPE=Release ..
----

4.{sp}Билдим far2l.
[source]
----
$ make -j4
----

5.{sp}Устанавливаем far2l.
[source]
----
$ sudo make install
----

=== Установка и настройка IDE Intelliji Idea

.Установка

1.{sp}Скачиваем intelliji idea.

Т.к. версия может отличаться дальнейшие шаги на примере последней актуальной на момент написания статьи.

2.{sp}Переходим в каталог со скачанным архивом и распаковываем его.
[source]
----
$ cd ~/Загрузки && tar -zxvf ideaIC-2020.1.3.tar.gz
----

3.{sp}Перемещаем в каталог приложений.
[source]
----
$ mv idea-IC-201.8538.31 ~/apps
----

4.{sp}Создаем ярлык для запуска.
[source]
----
echo '[Desktop Entry]
Encoding=UTF-8
Exec=/home/<username>/apps/idea-IC-201.8538.31/bin/idea.sh <1>
Icon=/home/<username>/apps/idea-IC-201.8538.31/bin/idea.png <1>
Type=Application
Terminal=false
Comment=IntellijIdea
Name=IntellijIdea
GenericName=IntellijIdea
StartupNotify=false
Categories=Development;' | sudo tee /usr/share/applications/intellijidea.desktop
----
<1> <username> – необходимо подставить пользователя под которым происходит работа

.Настройка

5.{sp}Запускаем IDE.

6.{sp}В стартовом окне запуска устанавливаем плагины.
[source,subs=normal]
----
menu:Settings[Plugins > erlang, Makefile support, AsciiDoc, BashSupport > Restart IDE]
----
erlang - обязательно, остальные по желанию.

7.{sp}Создаем проект для r (после того как перезапустилось IDE)
[source,subs=normal]
----
menu:Create New Project[erlang > btn:[next] > Project SDK > btn:[Add Erlang SDK] > выбираем erlang - ~/erlangs/20.3 > btn:[ok] > btn:[next] > Заполняем настройки проекта r (указаны ниже) > btn:[Finish]]
----

Настройки проекта r

* Project name - `r`
* Project location - `~/IdeaProjects/r`
* More Settings
** Module name - `r`
** Content root - выбираем каталог с r - `/home/<username>/develop/r` (<username> – имя пользователя под которым выполняется настройка)
** Module file location - выбираем каталог с r - `/home/<username>/develop/r`

8.{sp}Удаляем лишний каталог созданные Idea - src (в корне на одном уровне с rostell_*)

9.{sp}Подгружаем все каталоги rostell_* как модули intellij idea
[source,subs=normal]
----
menu:File[New > Module from Existing Sources > выбираем файл \*.iml в соответствующем каталоге rostell_* (например rostell_boot.iml) > btn:[ok]]
----
* Повторяем для всех каталогов rostell_*
* У каждого каталога должен отобразиться значек `e` и каталоги внутри отметяться по целевому назначению (модуль будет доступен для выбора в поиске ctrl+shift+f)

10.{sp}Импортируем настройки IntellijIdea (при необходимости). Настройки взяты с рабочей машины ceceron.
[source,subs=normal]
----
menu:File[Manage IDE Settings > Import Settings... > выбираем файл settings.jar > btn:[ok] > Выбираем компоненты для импорта (точно убираем SDK Table) > btn:[ok] > btn:[restart]]
----

Файл settings с машины ceceron: link:{attachmentsdir}/devpc_setup/ceceron_intellijidea_settings.jar[ceceron_intellijidea_settings.jar]

11.{sp}Включение git history.
[source,subs=normal]
----
menu:File[Settings > Version Control > выбираем строку с корнем репозитория r (/home/<username>/develop/r) > btn:[+] > btn:[apply] > btn:[ok]]
----

12.{sp}Настраиваем emacs и rebar.
[source,subs=normal]
----
menu:File[Settings > Erlang External Tools > выбираем Rebar -> Path - /home/<username>/bin/rebar; выбираем emacs -> Path - /usr/bin/emacs > btn:[apply] > btn:[ok]]
----

13.{sp}Настраиваем билд erlang'a.
[source,subs=normal]
----
menu:File[Settings > Build, Execution, Deployment > Compiler > Erlang Compiler > отмечаем Compile project with rebar > btn:[apply] > btn:[ok]]
----

=== Настройка asciidoc

1.{sp}Клонируем репозиторий ci (если еще не склонирован).
[source]
----
$ mkdir -p ~/develop && cd ~/develop && git clone https://bitbucket.org/rtrep/ci.git
----

%% @TODO описать после того как переведем pipeline docs_build на скрипты *.sh

=== Настройка r_role_node_connector

1.{sp}Добавляем alias.
[source]
----
$ echo 'alias rrnc="sudo <git_repo_path>"' >> ~/.bash_aliases <1>
----
<1> <git_repo_path> – путь до репозитория r.

Например
[source]
----
$ echo 'alias rrnc="sudo $HOME/develop/r/scripts/r_role_node_connector.sh"' >> ~/.bash_aliases
----

2.{sp}Обновляем алиасы для текущего терминала.
[source]
----
$ source ~/.bash_aliases
----

3.{sp}Теперь r_role_node_connector доступен для вызова из консоли, через команду rrnc
[source]
----
$ rrnc

==R Коннектор к ролевым нодам==
Введите номер ноды к которой необходимо подключиться
     1	ic1@10.0.2.15
     2	mdc1@10.0.2.15
     3	mic@10.0.2.15
     4	n@10.0.2.15
     5	rpci1@10.0.2.15
     6	rpco1@10.0.2.15
     7	store1@10.0.2.15
     8	testserv@10.0.2.15
     9	ws1@10.0.2.15
1
Attaching to /var/run/rostell/ic1@10.0.2.15/_erlang.pipe (^D to exit)
----

== Во время работы

=== Скрипт rostell

Описание команд доступно по ссылке (справочная документация по r):
http://docs.kzn/docs/develop/r/develop/descriptions/rostell_bin.html[]

=== Часто используемые команды make.

Makefile находится в корне каталога r.
Список часто используемых команд:
* ``make`` – запускает цель по умолчанию - билд + get-deps
* ``make xref`` – проверить кросс-референс ссылки
* ``make clean`` – очистить результаты билда r
* ``make ccx`` – запустить clean-compile-xref-eunit
* ``make eunit`` – запустить unit тесты
