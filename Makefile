all: setup

setup:
	mkdir -p ~/.config/rebar3/templates && ln -s `pwd` ~/.config/rebar3/templates/er2p

example:
	cd ../ && rebar3 new er2p name=project_name service_name="myService" author_name="Ceceron" author_email="llceceron@gmail.com" app_descr="ER2P Template Application"

clean_example:
	rm -rf ../project_name