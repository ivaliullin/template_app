%%% coding: utf-8
%%% @author {{author_name}} <{{author_email}}>
%%% @date {{date}}
%%% @doc

-module({{name}}_utils).
-author('{{author_name}} <{{author_email}}>').

-export([drop_last_subdir/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../../include/{{name}}.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------
-spec drop_last_subdir(Path::string(),N::integer()) -> NPath::string().
%% -------------------
drop_last_subdir(Path, 0) -> Path;
drop_last_subdir(Path, N)
  when is_integer(N) ->
    drop_last_subdir(
      lists:droplast(
        lists:reverse(
          lists:dropwhile(fun($/) -> false; (_) -> true end, lists:reverse(Path)))), N-1).

%% ====================================================================
%% Internal functions
%% ====================================================================