%%% coding: utf-8
%%% @author {{author_name}} <{{author_email}}>
%%% @date {{date}}
%%% @doc

-module({{name}}).
-author('{{author_name}} <{{author_email}}>').

-behaviour(application).

-export([start/0,stop/0]).
-export([start/2, stop/1]).
-export([add_deps_paths/0]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/{{name}}.hrl").

%% ====================================================================
%% Public API
%% ====================================================================

% ----
start() ->
    ?OUT("~s. start", [?ServiceName]),
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

% ----
stop() ->
    application:stop(?APP).

%% ===================================================================
%% Private
%% ===================================================================

% ----
start(_Mode, State) ->
    ?OUT("~s. start(Mode, State)", [?ServiceName]),
    add_deps_paths(),
    ?SUPV:start_link(State).

% ----
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

% ----
%% adds app's dependencies paths to code
add_deps_paths() ->
    _Path = ?U:drop_last_subdir(code:which(?MODULE), 3),
    Deps = [],
    code:add_pathsa(Deps).
