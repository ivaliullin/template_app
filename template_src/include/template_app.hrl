%% ====================================================================
%% Variables
%% ====================================================================

-define(APP, {{name}}).
-define(ServiceName, "{{service_name}}").

%% ====================================================================
%% Modules
%% ====================================================================

-define(SUPV, {{name}}_supv).
-define(SRV, {{name}}_srv).
-define(U, {{name}}_utils).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(OUT(Fmt,Args), io:format(Fmt++"~n",Args)).
-define(OUT(Text), io:format(Text++"~n",[])).