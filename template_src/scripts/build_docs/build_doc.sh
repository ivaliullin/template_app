#!/bin/bash

# for xref install - npm install -g gitlab:antora/xref-validator

export DOCSEARCH_ENABLED=true
export DOCSEARCH_ENGINE=lunr

case $1 in
docs)
    antora --stacktrace --clean --to-dir ../../_build/docs --generator antora-site-generator-lunr docs.yaml;;
xdocs)
    antora --stacktrace --clean --to-dir ../../_build/docs --generator @antora/xref-validator docs.yaml;;
*)
    antora --stacktrace --clean --to-dir ../../_build/docs --generator antora-site-generator-lunr docs.yaml;;
esac

